# rule-of-5

Это задача типа [crashme](https://gitlab.com/moskalenkoviktor/shad-cpp0-nn/blob/master/crash_readme.md).

Исходный код находится в файле `main.cpp`. Исполяемый файл получен командой

```
g++ main.cpp -o main -std=c++11
```
