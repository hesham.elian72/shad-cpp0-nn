# Reallol

Это задача типа [crashme](https://gitlab.com/moskalenkoviktor/shad-cpp0-nn/blob/master/crash_readme.md).

Исходный код находится в файле `main.cpp`. Исполяемый файл получен командой
```
g++ main.cpp -o reallol -std=c++11 -fsanitize=address
```

Советуем не писать ввод каждый раз руками, а сохранить его в файл и
пользоваться перенаправлением ввода
```
./reallol <input
```

Послать ввод из файла на сервер можно командой:
```
(echo reallol; sleep 1; cat input) | nc crashme.manytask.org 80
```
