#include <catch.hpp>

#include <sstream>
#include <string>

#include "tdd.h"

TEST_CASE("Tokenizer 1+2")
{
    std::stringstream ss{"1+2"};

    Tokenizer tok(&ss);

    REQUIRE(tok.GetType() == Tokenizer::NUMBER);
    REQUIRE(tok.GetNumber() == 1);

    tok.Consume();
    REQUIRE(tok.GetSymbol() == '+');
    REQUIRE(tok.GetType() == Tokenizer::SYMBOL);

    tok.Consume();
    REQUIRE(tok.GetType() == Tokenizer::NUMBER);
    REQUIRE(tok.GetNumber() == 2);

    tok.Consume();
    REQUIRE(tok.GetType() == Tokenizer::END);
}

TEST_CASE("Tokenizer handles whitespaces")
{
    for (auto input : {" ", "\t", " \t", "\t\t"}) {
        std::stringstream ss{input};
        Tokenizer tok(&ss);

        WHEN("When input is " + input) {
            REQUIRE(tok.GetType() == Tokenizer::END);
        }
    }
}

TEST_CASE("Parse and evaluate")
{
    for (auto test_case : {
        std::make_pair("1", 1),
        std::make_pair("-2", -2),
        std::make_pair("2+2", 4),
        std::make_pair("2*2", 4),
    }) {
        std::stringstream ss{test_case.first};
        Tokenizer tok(&ss);

        auto number = Parse(&tok);
        REQUIRE(number->Evaluate() == test_case.second);
    }
}
