# Курс C++

Это репозиторий курса в Нижнем Новгороде. Инструкции по настройке окружения описана в [SETUP.md](docs/SETUP.md). Структура семинарских задач приведена [в тестовой задаче](https://gitlab.com/moskalenkoviktor/shad-cpp0-nn/tree/master/multiplication). Задачи типа crashme описаны [здесь](https://gitlab.com/moskalenkoviktor/shad-cpp0-nn/blob/master/crash_readme.md).

Перемещаться по задачам и следить за дедлайнами можно тут: https://cpp0-nn.manytask.org/
