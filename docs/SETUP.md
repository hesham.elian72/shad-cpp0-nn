# Настройка окружения

Для решения задач вам потребуется компьютер с unix окружением. Лучше всего использовать linux.
OSX тоже будет нормально работать.

Можно использовать Windows, но для компиляции и запуска нужно будет использовать WSL:
  * [CLion](https://www.jetbrains.com/help/clion/how-to-use-wsl-development-environment-in-clion.html)
  * [VS Code](https://code.visualstudio.com/docs/cpp/config-wsl)

Мы рекомендуем использовать Ubuntu версии **20.04**. Мы проверяем все задачи на этой
версии убунты, и если вы будете использовать эту же версию локально, то вы избежите
проблем со старыми версиями компиляторов.

## Регистрация в системе

1. Зарегистрируйтесь в тестовой системе. https://cpp0-nn.manytask.org/ Код регистрации доступен [на странице курса](https://lk.yandexdataschool.ru/courses/2021-autumn/10.1002-obuchenie-iazyku-cpp-chast-1/)
2. Сгенерируйте ssh ключ, если у вас его еще нет.
   ```
   ssh-keygen -N "" -f ~/.ssh/id_rsa
   ```
3. Скопируйте содержимое файла id_rsa.pub (`cat ~/.ssh/id_rsa.pub`) в https://gitlab.manytask.org/profile/keys
4. Проверьте, что ssh ключ работает. Выполните команду `ssh git@gitlab.manytask.org`. Вы должны увидеть такое приветствие:
   ```
   $ ssh git@gitlab.manytask.org
   PTY allocation request failed on channel 0
   Welcome to GitLab, Fedor Korotkiy!
   Connection to gitlab.manytask.org closed.
   ```

## Посылка задачи в систему

1. Склонируйте репозиторий с задачами.
   ```
   git clone https://gitlab.com/moskalenkoviktor/shad-cpp0-nn
   ```

   Команда `git clone` создаст директорию `shad-cpp0-nn` и запишет туда все файлы из этого репозитория.
   Во время курса, мы будем обновлять тесты и условия задач. Перед началом каждого семинара, вам нужно
   будет обновлять свою локальную копию репозитория. Если этого не сделать, вы можете потратить
   большое время на разбирательства с багом, который мы уже починили.

2. Настройте IDE и решите первую задачу [multiplication](https://gitlab.com/moskalenkoviktor/shad-cpp0-nn/-/tree/master/multiplication).

   У вас есть много вариантов, в чем писать код. Мы рекомендуем использовать Visual Studio Code.

3. Сдайте тестовое задание в систему.

 - Настройте гит.
   ```
   git config --global user.name "Vasya Pupkin"
   git config --global user.email vasya@pupkin.ru
   ```

 - Откройте страницу своего репозитория в браузере. Перейдите по ссылке MY REPO на странице с задачами. https://cpp0-nn.manytask.org/

 - Добавьте в git свой приватный репозиторий. Для этого запустите из директории репозитория команду:

   ```
   git remote add student ADDRESS
   ```

   `ADDRESS` нужно скопировать со страницы репозитория. Синяя кнопка Clone -> Clone with SSH.

 - Запустите скрипт посылки из директории с задачей. `python3 ../submit.py`.

 - Пронаблюдайте за процессом тестирования со страницы CI/CD -> Pipelines своего репозитория. gitlab показывает вывод консоли во время тестирования.

 - Проверьте, что оценка появилась в [таблице с результатами](https://docs.google.com/spreadsheets/d/11l3kPthDgQ4qpqIrAC0jPMlUTl8vEqh-Ljuf59SGlao)

4. Периодически мы будем обновлять репозиторий с задачами, поэтому перед каждым семинаром необходимо выполнять
   следующие 3 команды из вашего локального репозитория (shad-cpp0-nn):

   ```
   git stash
   git pull
   git stash pop
   ```
