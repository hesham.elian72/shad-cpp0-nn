#pragma once

#include <string>
#include <memory>
#include <optional>
#include <stdexcept>

struct Location {
    double lat, lon;
};

struct WeatherForecast {
    double temp = 0;
    double feels_like = 0;
};

struct YandexAPIError : public std::runtime_error {
    YandexAPIError(int http_code_p, const std::string& details_p)
        : std::runtime_error("api error: code=" + std::to_string(http_code_p) +
                             " details=" + details_p),
          http_code(http_code_p),
          details(details_p) {
    }

    int http_code;
    std::string details;
};

class IForecaster {
public:
    virtual ~IForecaster() = default;

    virtual WeatherForecast ForecastWeather(std::optional<Location> where) = 0;
};

std::shared_ptr<IForecaster> CreateYandexForecaster(
    const std::string& api_key,
    const std::string& api_endpoint = "http://api.weather.yandex.ru/v1/forecast");
