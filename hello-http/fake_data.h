#pragma once

#include <string>

struct FakeData {
    static std::string moscow_weather;
    static std::string spb_weather;
};