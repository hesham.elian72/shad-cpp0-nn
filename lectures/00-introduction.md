# Лекция 1. Введение в C++

Лев Высоцкий

---

# Вы пришли на курс С++

 - Занятия по вторникам
 - Лекции и семинары

---

# Задания в курсе

 - 12+ семинаров, дедлайн 10 дней. После дедлайна штраф 70%
 - 3 домашних задания, **жёсткий** дедлайн 2-3 недели
 - Критерии оценки &mdash; на странице курса.

---

# Телеграм-чат

 - Есть у вас есть вопрос или возникла какая-то проблема &mdash; задавайте вопрос в чате.
 - Флуд не приветствуется
 - Логи, вывод компилятора и большие куски кода заливайте на `gist.github.com`

---

# C++
 - Язык очень объёмный
 - Многие части языка реализованы в библиотеках
 - Каждый крупный проект использует подмножество языка
 - **Вам не нужно знать весь С++, чтобы успешно его применять**

---

<!-- # Бъерн Страуструп - Создатель С++

![](00-introduction/straustruprm.jpg)

--- -->

# Стандарт С++

 - Описание языка и стандартной библиотеки
 - Создан комитетом по стандартизации
 - 1834 страниц


---

# История стандартов C++

 - с++98 &mdash; вам не встретится
 - с++11 &mdash; добавил очень много вещей, язык сильно поменялся
 - с++14 &mdash; небольшие правки (по сути c++11.1)
 - с++17 &mdash; c++11.2
 - c++20 &mdash; последний принятый стандарт, масштабные изменения

---

# Компиляторы

 * **gcc** - основной компилятор на linux, лицензия GPL.
 * **clang** - основной компилятор на OSX и iOS, лицензия MIT.
 * **mvcc** - основной компилятор на Windows.
 * **icc** - компилятор от Intel, проприетарный.

Компиляторы одновременно поддерживают несколько версий стандарта.
`clang -std=c++20 ...`

---

# Hello World

```c++
#include <iostream>

int main() {
    std::cout << "Hello World\n";
}
```

---

# Переменные

```c++
int my_variable;
int another = 0;
```

---

# Примитивные типы

```
bool the_cake_is_a_lie = true;

int answer_to_the_question = 42;

size_t unsigned_number = 1U;
int64_t signed_number = -1LL;
uint64_t quite_big_unsigned_number = 1'000'000'000'000ULL;

float floating_point_number = 0.5f;
double double_precision_floating_point_number = 1e9;

char yes = 'y';
```

---

# Арифметические операторы

```c++
int x, y, z;
int a = x + y * z - 12 / z;
a *= z;
a += x - y;
--a;
a++;
int b = x % 2;
```

---

# Операторы сравнения

```c++
int x, y, z;
bool equal = (x == y);
bool not_equal = (x != y);
bool greater = y > z;
bool less = x < z;
bool greater_or_equal = (x >= z);
```

---

# Логические операторы

```c++
bool pravda = true;
bool lozh = false;
bool logical_or = pravda || lozh;
bool logical_and = pravda && lozh;
bool nepravda = !pravda;
```

### Short-circuit evaluation

`&&` и `||` вычисляются по особым правилам:

```c++
bool b = president_is_alive || LaunchNukes();
```

---

# Битовые операторы

```c++
uint64_t x, y, z;
uint64_t bit_or = x | y;
uint64_t bit_and = x & z;
uint64_t bit_xor = y ^ z;
uint64_t bit_complement = ~x;
```

---

# auto и вывод типов

Ключевое слово `auto` позволяет не писать тип

```с++
auto x = 12; // int
auto y = x; // int
auto b = (x == y); // bool
auto d = 3.14159; // double
auto f = 3.14159f; // float
```

---

# Ввод и вывод (`<iostream>`)

```c++
int a, b;
char c;
std::cin >> a >> b >> c;
std::cout << (a + b) << ' ' << c << std::endl;
```

Ввод:
```
1 2 Z
```

Вывод:
```
3 Z
```

---

# Функции

```c++
int Sum(int a, int b) {
    int c = a + b;
    return c;
}
```

---

# Функции

```c++
// Определение (definition)
int Sum(int a, int b) {
    int c = a + b;
    return c;
}

// Объявление (declaration)
int Square(int b);

void F() {
    // Вызов (call)
    std::cout << Sum(1, 2) << std::endl;    
    int a = Square(-1);
}
```

---

# Control flow. Оператор if

```c++
void PrintSign(int i) {
    if (i > 0) {
        std::cout << "positive" << std::endl;
    } else if (i < 0) {
        std::cout << "negative" << std::endl;
    } else {
        std::cout << "zero" << std::endl;
    }
}
```

---

# Control flow. Оператор for

```c++
int Factorial(int n) {
    int f = 1;
    for (int i = 1; i <= n; ++i) {
        f *= i;
    }
    return f;
}
```

---

# Control flow. Оператор while

```c++
int SumOfDigits(int n) {
    int s = 0;
    while (n != 0) {
        s += (n % 10);
        n /= 10;
    }
    return s;
}
```

---

# Рекурсия

```c++
int Factorial(int n) {
    if (n <= 1) {
        return 1;
    }
    return Factorial(n - 1) * n;
}
```

---

# Рекурсия

```c++
void F2(); // forward declaration

void F1() {
    ...
    F2();
}

void F2() {
    ...
    F1();
}
```

---

# `std::vector<T>`

 - Динамический массив элементов типа `T`
 - Индексация с нуля
 - Обращение к элементу по индексу за O(1)
 - Добавление элемента в конец за O(1)

---

# `std::vector<T>`

```c++
std::vector<int> years;
years.push_back(2019); // years: [2019]
years.push_back(2020); // years: [2019, 2020]

years[0] = 1812; // years: [1812, 2020]

std::cout << years.size() << std::endl;

years.pop_back(); // years: [1812]

std::cout << years.size() << std::endl;
```

---

# Итерация по вектору

```c++
void PrintVector(std::vector<int> numbers) {
    for (int x : numbers) {
        std::cout << x << std::endl;
    }
}

void PrintVectorOld(std::vector<int> numbers) {
    for (int i = 0; i < std::ssize(numbers); ++i) {
        std::cout << numbers[i] << std::endl;
    }
}
```

---

# Пример работы с вектором

```c++
std::vector<int> Remove42Suffix(std::vector<int> v) {
    while (!v.empty() && v.back() == 42) {
        v.pop_back();
    }
    return v;
}
```

---

# `std::string`
Последовательность элементов типа `char`

```c++
std::string name = "Fedor";
std::string login = "prime";
name.size() == 5;
name[1] == 'e';

std::string display_name = name + " aka " + login;
```

---

# `std::unordered_map<K, V>`

 - Отображение из объектов типа `K` в объекты типа `V`
 - Поиск по `K`, добавление и удаление за O(1)

---

# `std::unordered_map<K, V>`

```c++
std::unordered_map<std::string, int> word_counts;
word_counts["The"] = 10000;
word_counts["a"] = 50;

word_counts["a"] == 50; // true
word_counts["b"] == 0; // true

std::string key = "c";
word_counts.emplace(key, 200);

if (word_counts.contains(key)) {
    std::cout << key << " " << word_counts[key] << std::endl;
}

word_counts.erase("The");
word_counts.size() == 3;
```

---

# Итераторы

 - "Указывают" на элементы контейнера
 - Передаются во многие библиотечные функции

```c++
std::vector<int> v = {3, 2, 1};
std::sort(v.begin(), v.end());
// v: [1, 2, 3]
```

---

# Что читать
 - Документация: [https://en.cppreference.com](https://en.cppreference.com)
 - Для начинающих: Bjarne Stroustrup *A Tour of C++ (2nd ed.)*
 - Для продолжающих: Scott Meyers *Effective C++ (3rd ed.)*, *Effective Modern C++*, *Effective STL*