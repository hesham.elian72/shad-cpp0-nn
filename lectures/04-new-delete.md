---
marp: true
---

# Динамическое управление памятью

---

## Вспоминаем: Указатели

```c++
int a = 1, b = 2;

int* p = &a;

*p = 3;

p = &b;

*p = 4;
```

---

## Node based stack

```c++
struct Node {
    int value = 0;
    Node* next = nullptr;
};

void F() {
    Node n1{1, nullptr}, n2{2, nullptr}, n3{3, nullptr};

    n1->next = &n2;
    n2->next = &n3;
}
```

---

## Неправильный Push

```c++
struct Node {
    int value = 0;
    Node* next = nullptr;
};

class Stack {
public:
    void Push(int value) {
        Node n{value, head_};

        head_ = &n; // wrong
    }

private:
    Node* head_;
};
```

---

## Время жизни

```c++
void f(Node y)
{
    Node x;

    {
        Node z;

        // z.~Node();
    }

    // x.~Node();
    // y.~Node();
}
```

---

## Автоматические переменные

- Живут "на стеке"
- Привязаны к определенному блоку
- Уничтожаются после выхода из блока

---

## Операторы `new` и `delete`

- `new` позволяет выделять объекты "в куче"

```c++
std::string* s = new std::string{};
```

- `delete` освобождает объект, который ранее выделили через `new`

```c++
delete s;
```

- Реализации контейнеров используют `new`
- В production коде используются высокоуровневые аналоги

---

## Правильный Push

```c++
struct Node {
    int value = 0;
    Node* next = nullptr;
};

class Stack {
public:
    void Push(int value) {
        // Node n{value, head_};
        Node* n = new Node{value, head_};

        head_ = n;
    }

private:
    Node* head_;
};
```

---

## Ошибки при работе с `new`

Оператор `new` создаёт новый объект и возвращает указатель на него. Объект нужно разрушить через `delete`, после этого обращаться к объекту нельзя.

- **Ошибка #1:** Обратились к объекту после `delete`. *use after free*
- **Ошибка #2:** Не позвали `delete`. *memory leak*

Как бороться с этими ошибками?

- **Ошибка #1:** Не обращаться к объекту после `delete`. *Git gud*
- **Ошибка #2:** Использовать *ownership* и *RAII*.

---

## Кто должен освобождать память?

```c++
void DoSomething(Node *node);

void DoOtherThing() {
    auto n = new Node{};
    n->value = 1;

    DoSomething(n);
    // delete n; <= option A
}
```

```c++
void DoSomething(Node* node) {
    std::cout << node->value << std::endl;
    // delete node; <= option B
}
```

---

## Вариант C: Копирование

```c++
void DoOtherThing() {
    auto n = new Node{};
    n->value = 1;

    DoSomething(new Node{*n});
    delete n;
}
```

```c++
void DoSomething(Node* node) {
    std::cout << node->value << std::endl;
    delete node;
}
```

---

## Вариант A без `new`

```c++
void DoOtherThing() {
    Node n;
    n.value = 1;

    DoSomething(&n);
}
```

```c++
void DoSomething(Node* node) {
    std::cout << node->value << std::endl;
}
```

---

## Вариант C без `new`

```c++
void DoOtherThing() {
    Node n;
    n.value = 1;

    DoSomething(n);
}
```

```c++
void DoSomething(Node node) {
    std::cout << node.value << std::endl;
}
```

---

## Вариант B без `new`

```c++
void DoOtherThing() {
    Node n;
    n.value = 1;

    DoSomething(std::move(n));
}
```

```c++
void DoSomething(Node node) {
    std::cout << node.value << std::endl;
}
```

---

## Кто же должен освобождать память?

0. Кто выделил, тот и освобождает.
1. Если он не передал владение другому.
2. Если другой сделал себе копию, он освобождает её сам.

---

## Деструктор

```c++
class User
{
public:
    User(const std::string& user)
        : name_(new std::string{user})
    { }

    ~User()
    {
        delete name_;
    }

private:
    std::string* name_;
};

void f() {
    User u("Fedor");
}   // ~User вызывается в конце жизни объекта
```

---

## Копирование

```c++
class User
{
public:
    User(const User& other);
    User(User&& other);
    User& operator = (const User& other);
    User& operator = (User&& other);

    ~User();
};

void f() {
    User u1, u2;

    u2 = u1; // user defined copy assignment

    User u3{u1}; // user defined copy construction
}
```

---

## Перемещение

```c++
class User
{
public:
    User(const User& other)
        : name_(new std::string{*other.name_})
    { }

    User(User&& other)
        : name_(other.name_)
    {
        other.name_ = nullptr;
    }
}

void f()
{
    User tmp = Login();

    User me = std::move(tmp); // что произойдёт?
}
```

---

## Правило пяти

```c++
class User
{
public:
    User(const User& other);
    User(User&& other);
    User& operator = (const User& other);
    User& operator = (User&& other);
    ~User();
};
```

- Если вы определили один из 5 методов, вы должны опеределить все 5
- В современном С++ действует правило нуля, но мы поговорим про него чуть позже

---

## Ownership graph

```c++
struct User {
    std::string name;
};

std::vector<std::map<int, User>> m;
std::vector<User*> q;
```

---

## `new[]` и `delete[]`

```c++
std::string* s = new std::string[10]{};

delete[] s;
```

- Выделяют и освобождают сразу много элементов
- Эта форма оператора вам понадобится 1 раз в жизни :)

---

## Арифметика указателей

```c++
std::string* s = new std::string[10]{};

s[1]; // первый элемент
*(s + 1); // тоже первый элемент

delete[] s;
```

---

## Ошибки при работе с `[]new`

- **Ошибка #1:** Обратились к объекту после `delete[]`. *use after free*
- **Ошибка #2:** Не позвали `delete[]`. *memory leak*
- **Ошибка #3:** Обратились за границу массива. *buffer overflow*

---

## And now for something completely different

Read from unexecuted branch.

```c++
r1 = x.load(memory_order_relaxed);
y.store(r1, memory_order_relaxed);
```

```c++
bool assigned_42 = false;
r2 = y.load(memory_order_relaxed);
if (r2 != 42) {
    assigned_42 = true;
    r2 = 42;
}
x.store(r2, memory_order_relaxed);
assert_not(assigned_42);
```
