# Casul Sort

Это задача типа [crashme](https://gitlab.com/moskalenkoviktor/shad-cpp0-nn/blob/master/crash_readme.md).

Исходный код находится в файле run.cpp. Исполняемый файл получен командой
```
g++ -std=c++17 -nostdinc++ -I/var/build/llvm-build/include/c++/v1 -L/var/build/llvm-build/lib -Wl,-rpath,/var/build/llvm-build/lib \
         run.cpp -nodefaultlibs -lc++ -lc++abi -lm -lc -lgcc_s -lgcc -O2 -o run
```

Здесь используется реализация [libc++](https://libcxx.llvm.org/docs/index.html) стандартной библиотеки (версия 10). Если у вас
OS X Mavericks или новее, то вы можете компилировать как обычно (поскольку там она используется по умолчанию):
```
g++ -std=c++17 -O2 run.cpp -o run
```

Чтобы скомпилировать под Linux, может пригодиться документация по [сборке](https://libcxx.llvm.org/docs/BuildingLibcxx.html).

Вам, скорее всего, потребуется отправить большой ввод, поэтому запишите его в файл и используйте перенаправление ввода
```
./run <input
```

Послать ввод из файла на сервер можно командой:
```
(echo casul-sort; sleep 1; cat input) | nc crashme.manytask.org 80
```
