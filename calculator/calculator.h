#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <memory>

class Tokenizer {
public:
    Tokenizer(std::istream* in) : in_(in) {
    }

    enum TokenType { kUnknown, kNumber, kSymbol, kEnd };

    void Consume() {
    }

    TokenType GetType() {
    }

    int64_t GetNumber() {
    }

    char GetSymbol() {
    }

private:
    std::istream* in_;

    TokenType type_ = TokenType::kUnknown;
    int64_t number_;
    char symbol_;
};

class Expression {
public:
    virtual ~Expression() {
    }
    virtual int64_t Evaluate() = 0;
};

std::unique_ptr<Expression> ParseExpression(Tokenizer* tok) {
    return nullptr;
}
