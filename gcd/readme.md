# GCD

Это задача типа [crashme](https://gitlab.com/moskalenkoviktor/shad-cpp0-nn/blob/master/crash_readme.md).

Исходный код находится в файле gcd.cpp. Исполяемый файл получен командой
```
g++ -std=c++17 -O0 -Wall gcd.cpp -o gcd
```
