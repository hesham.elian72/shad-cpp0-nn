#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 * compiled with:
 * gcc -m32 -O0 -fno-stack-protector stack1.c -o stack1
 */

char* exec_string = "/bin/sh";

void shell(char* cmd)
{
	execl(cmd, cmd, NULL);
        perror("exec");
}

void print_name(char* input)
{
	char buf[15];
	strcpy(buf, input);
	printf("Hello %s\n", buf);
}

int main(int argc, char** argv)
{
	if(argc != 1)
	{
		printf("usage:\n%s\n", argv[0]);
		return EXIT_FAILURE;
	}

        char input[1024];
        bzero(input, sizeof(input));
        fgets(input, 1023, stdin);
        
	print_name(input);

	return EXIT_SUCCESS;
}
